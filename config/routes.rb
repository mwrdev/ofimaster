Rails.application.routes.draw do

  resources :posts
  resources :slides
  get 'admin/dashboard'

  devise_for :users
  
  get 'orders/create'

  resources :products
  #resources :users
  resources :orders

  root 'pages#index'

  get '/cart' => 'cart#index'
  get '/cart/clear' => 'cart#clear'
  post '/cart/add' => 'cart#add'

  get '/sincronizar' => 'products#sincronizar'
  get '/sincronizarupdate' => 'products#sincronizarupdate'
  get '/dashboard' => 'admin#dashboard'
  get '/cuenta' => 'pages#account'
  get '/verificar' => 'pages#verificar'
  get '/newcustomer' => 'pages#newcustomer'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
