require 'test_helper'

class CartControllerTest < ActionDispatch::IntegrationTest
  test "should get add" do
    get cart_add_url
    assert_response :success
  end

  test "should get clear" do
    get cart_clear_url
    assert_response :success
  end

  test "should get index" do
    get cart_index_url
    assert_response :success
  end

end
