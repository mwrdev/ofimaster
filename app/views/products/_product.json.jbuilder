json.extract! product, :id, :productid, :code, :description, :familyid, :familyname, :unitprice, :unitpricewithtaxes, :barcode, :notes, :created_at, :updated_at
json.url product_url(product, format: :json)
