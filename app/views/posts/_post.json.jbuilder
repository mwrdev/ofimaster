json.extract! post, :id, :title, :slug, :description, :content, :image, :created_at, :updated_at
json.url post_url(post, format: :json)
