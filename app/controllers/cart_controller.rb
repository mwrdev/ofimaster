class CartController < ApplicationController
  before_action :authenticate_user!
  
  def add
  	#id = params[:id]
    #quantity = params[:quantity].to_i
    count = 1
    params.each do
      
  	  id = params["id#{count}"]
      quantity = params["quantity#{count}"].to_i
      
      if quantity>0
        cart[id] ? cart[id] = cart[id] + quantity : cart[id] = quantity
      end

      count+=1
    end
    #cart[id] ? cart[id] = cart[id] + 1 : cart[id] = 1

  	flash[:notice] = 'Productos Agregados al Carro'
  	redirect_to :back
  end

  def delete
    
  end

  def clear
  	session[:cart] = nil
  	redirect_to action: :index
  end

  def index
  	@products = Product.all
    
    array_cart = cart.to_a
  	@cart = array_cart
    session[:cart] = cart
  end
end
