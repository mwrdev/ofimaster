class PagesController < ApplicationController
  before_action :authenticate_user!, only: [:account]

  def index
    @slides = Slide.all
  end

  def verificar
    respond_to :html, :js

    api_key = HTTParty.post('https://erp.laudus.cl/api/users/login?user=ibarrios&password=ibarrios&companyVatId=78307160-6', :headers => {'Accept' => 'application/json'})
    $token = JSON.parse api_key.body
    
    rutempresa = params[:rutempresa]
  
    customerid = HTTParty.get(
        "https://erp.laudus.cl/api/customers/get/customerId/byVatId/#{rutempresa}", 
        :headers => $token
    )
    customerid_parse = JSON.parse customerid.body
    @customerid = customerid_parse

    category = HTTParty.get(
        "https://erp.laudus.cl/api/customers/families/get/list", 
        :headers => $token
    )
    category_parse = JSON.parse category.body
    @category = category_parse

  end

  def newcustomer
    respond_to :html, :js

    api_key = HTTParty.post('https://erp.laudus.cl/api/users/login?user=ibarrios&password=ibarrios&companyVatId=78307160-6', :headers => {'Accept' => 'application/json'})
    $token = JSON.parse api_key.body
  end

  def account
    if $token.nil?
      api_key = HTTParty.post('https://erp.laudus.cl/api/users/login?user=ibarrios&password=ibarrios&companyVatId=78307160-6', :headers => {'Accept' => 'application/json'})
      $token = JSON.parse api_key.body
      
      customerid = current_user.customerid
    
      customer = HTTParty.get(
          "https://erp.laudus.cl/api/customers/get/#{customerid}", 
          :headers => $token
      )
      customer_parse = JSON.parse customer.body
      @customer = customer_parse

      credit = HTTParty.get(
          "https://erp.laudus.cl/api/customers/get/creditLimit/#{customerid}", 
          :headers => $token
      )
      credit_parse = JSON.parse credit.body
      @credit = credit_parse
    else
      
      customerid = current_user.customerid
    
      customer = HTTParty.get(
          "https://erp.laudus.cl/api/customers/get/#{customerid}", 
          :headers => $token
      )
      customer_parse = JSON.parse customer.body
      @customer = customer_parse

      credit = HTTParty.get(
          "https://erp.laudus.cl/api/customers/get/creditLimit/#{customerid}", 
          :headers => $token
      )
      credit_parse = JSON.parse credit.body
      @credit = credit_parse
    end
  end

end