class OrdersController < ApplicationController
  before_action :authenticate_user!
  
  def show
  	@order = Order.find(params[:id])
  	@products = Product.all
  end

  def index
  	#@orders = Order.all
    #@user = current_user
    @orders = Order.where(user_id: current_user.id).page(params[:page]).search(params[:pedido])
    #@orders = @user.orders
    @products = Product.all
  end

  def create
  	@order = Order.new
  	@order.items = session[:cart].to_a
    @order.user_id = current_user.id

  	@products = Product.all
  	@email = current_user.email

	
=begin
  	mail = OrderMailer.order_email(@order, @products, @email)

	if mail.deliver_now

		flash[:notice] = 'Pedido enviado'
		redirect_to root_path
	#else
	#	flash[:error] = 'Error to send order'
	#	redirect_to root_path
	end
=end

    respond_to do |format|
	  if @order.save
	    # Sends email to order when order is created.
	    session[:cart] = nil

		mail = OrderMailer.order_email(@order, @products, @email)
		mail.deliver_now

	    format.html { redirect_to @order, notice: 'Pedido realizado.' }
	    format.json { render :show, status: :created, location: @order }
	  else
	    format.html { render :new }
	    format.json { render json: @order.errors, status: :unprocessable_entity }
	  end
	end
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_order
      @order = Product.find_by_id(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def order_params
      params.require(:order).permit(:items, :user_id)
    end

end
