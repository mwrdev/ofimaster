class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?

  #Makes cart available in views.
  helper_method :cart
  # Ensure cart session hash exists, if not, it creates a new one with its Value to a blank hash.
  
  def cart  
    session[:cart] ||= {}
  end

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:customerid, :rol])
    devise_parameter_sanitizer.permit(:account_update, keys: [:customerid, :rol])
  end

end
