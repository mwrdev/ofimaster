class AdminController < ApplicationController
  before_action :authenticate_user!

  def dashboard
    @users = User.all
    @orders = Order.all
    @products = Product.all
  end
end
