class ProductsController < ApplicationController
  before_action :set_product, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!, only: [:new, :edit, :update, :destroy, :sincronizar, :sincronizarupdate]

  def sincronizar
    api_key = HTTParty.post('https://erp.laudus.cl/api/users/login?user=ibarrios&password=ibarrios&companyVatId=78307160-6', :headers => {'Accept' => 'application/json'})

    token = JSON.parse api_key.body

    products = HTTParty.get(
        'https://erp.laudus.cl/api/products/get/list/complete', 
        :headers => token
    )

    products_parse = JSON.parse products.body

    products_parse.each do |product|
      Product.create(
        productid:          product["productId"], 
        code:               product['code'], 
        description:        product['description'], 
        familyid:           product['familyId'], 
        familyname:         product['familyName'], 
        unitprice:          product['unitPrice'], 
        unitpricewithtaxes: product['unitPriceWithTaxes'], 
        discontinued:       product['discontinued'], 
        barcode:            product['barcode'], 
        unit:               product['unit'], 
        notes:              product['notes']
      )
    end
    redirect_to root_path
  end


  def sincronizarupdate
    api_key = HTTParty.post('https://erp.laudus.cl/api/users/login?user=ibarrios&password=ibarrios&companyVatId=78307160-6', :headers => {'Accept' => 'application/json'})

    token = JSON.parse api_key.body

    products = HTTParty.get(
        'https://erp.laudus.cl/api/products/get/list/complete', 
        :headers => token
    )

    products_parse = JSON.parse products.body

    products_parse.each do |product|
      p = Product.find_by(code: product['code'].to_s)
      p.productid           = product["productId"]
      p.code                = product['code']
      p.description         = product['description']
      p.familyid            = product['familyId']
      p.familyname          = product['familyName']
      p.unitprice           = product['unitPrice']
      p.unitpricewithtaxes  = product['unitPriceWithTaxes']
      p.discontinued        = product['discontinued']
      p.barcode             = product['barcode']
      p.unit                = product['unit']
      p.notes               = product['notes']
      p.save
    end
    redirect_to root_path
  end

  # GET /products
  # GET /products.json
  def index
    @products = Product.all.page(params[:page]).search(params[:search])
  end

  # GET /products/1
  # GET /products/1.json
  def show
    @relacionados = Product.relacionados(@product)
  end

  # GET /products/new
  def new
    @product = Product.new
  end

  # GET /products/1/edit
  def edit
  end

  # POST /products
  # POST /products.json
  def create
    @product = Product.new(product_params)

    respond_to do |format|
      if @product.save
        format.html { redirect_to @product, notice: 'Product was successfully created.' }
        format.json { render :show, status: :created, location: @product }
      else
        format.html { render :new }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /products/1
  # PATCH/PUT /products/1.json
  def update
    respond_to do |format|
      if @product.update(product_params)
        format.html { redirect_to @product, notice: 'Product was successfully updated.' }
        format.json { render :show, status: :ok, location: @product }
      else
        format.html { render :edit }
        format.json { render json: @product.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /products/1
  # DELETE /products/1.json
  def destroy
    @product.destroy
    respond_to do |format|
      format.html { redirect_to products_url, notice: 'Product was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_product
      @product = Product.find_by_slug(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def product_params
      params.require(:product).permit(:photo, :productid, :code, :slug, :description, :familyid, :familyname, :unitprice, :unitpricewithtaxes, :discontinued, :barcode, :notes, :unit)
    end
end
