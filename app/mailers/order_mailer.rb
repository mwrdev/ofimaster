class OrderMailer < ApplicationMailer
	default from: "sergioazoc@gmail.com"
	
	def order_email(order, products, email)
	  @order = order
	  @products = products
	  @email = email
	  mail(to: 'sergioazoc@gmail.com', subject: 'Order Email')
	end
end
