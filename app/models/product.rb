class Product < ApplicationRecord
	mount_uploader :photo, PhotoUploader

	def self.search(search)
	    if search
	      Product.where("(discontinued = 'false' AND unitprice > 0) AND code ILIKE ? OR description ILIKE ? OR familyname ILIKE ? OR notes ILIKE ?", "%#{search}%", "%#{search}%", "%#{search}%", "%#{search}%").order(:familyname)
	    else
	      Product.all.where("discontinued = 'false' AND unitprice > 0").order(:unitprice)
	    end
	end

	#productos relacionados
	scope :relacionados, ->(product) { where("familyname = ?", product.familyname).order("RANDOM()").first(3) }

	

	after_create :update_slug
	before_update :assign_slug

	def to_param
		slug
	end

	private

	def assign_slug
		self.slug = "#{code.parameterize}"
	end

	def update_slug
		update_attributes slug: assign_slug
	end
end
