class Order < ApplicationRecord
	belongs_to :user

	def self.search(search)
	    if search
	      Order.where("id = ?", "#{search}")
	    else
	      Order.all
	    end
	end

end
