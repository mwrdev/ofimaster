class CreateSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :slides do |t|
      t.string :link
      t.string :image
      t.string :title

      t.timestamps
    end
  end
end
