class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.integer :productid
      t.string :code
      t.string :slug
      t.string :description
      t.string :familyid
      t.string :familyname
      t.integer :unitprice
      t.integer :unitpricewithtaxes
      t.boolean :discontinued
      t.string :barcode
      t.string :unit
      t.string :notes

      t.timestamps
    end
  end
end
